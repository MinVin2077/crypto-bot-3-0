'''import ccxt
import telebot
BOT_TOKEN = '6940841514:AAE0dYinB_aWRoZEAjPoFKqV6ursOaaqd04'

bot = telebot.TeleBot(BOT_TOKEN)
def get_crypto_price(symbol):
    exchange = ccxt.binance()
    ticker = exchange.fetch_ticker(symbol)
    return ticker['last']


@bot.message_handler(commands=['start', 'help'])
def start(message):
    help_text = (
        'Привет! Я бот для отображения курса криптовалют.\n\n'
        'Используй команду /crypto "символ", чтобы получить курс.\n\n'
        'Поддерживаемые символы криптовалют:\n'
        'btcusdt - Bitcoin в USDT\n'
        'ethusdt - Ethereum в USDT\n'
        'xrpusdt - Ripple в USDT\n'
        'ltcusdt - Litecoin в USDT\n'
        'Другие токены тоже поддерживаются\n'
        'Product by MinVin (Igor Minenkov)'
    )
    bot.reply_to(message, help_text)


@bot.message_handler(commands=['crypto'])
def crypto(message):
    if len(message.text.split()) == 1:
        bot.reply_to(message, 'Пожалуйста, укажите символ криптовалюты. Например, /crypto btcusdt')
        return

    crypto_symbol = message.text.split()[1].upper()

    try:
        price = get_crypto_price(crypto_symbol)
        bot.reply_to(message, f'Текущий курс {crypto_symbol.upper()}: ${price}')
    except Exception as e:
        bot.reply_to(message, f'Произошла ошибка: {e}')


if __name__ == '__main__':
    bot.polling(none_stop=True)
'''
import ccxt
import telebot
from telebot import types

BOT_TOKEN = '6940841514:AAE0dYinB_aWRoZEAjPoFKqV6ursOaaqd04'

bot = telebot.TeleBot(BOT_TOKEN)

def get_crypto_price(symbol):
    exchange = ccxt.binance()
    ticker = exchange.fetch_ticker(symbol)
    return ticker['last']

def create_crypto_keyboard():
    keyboard = types.ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
    buttons = [
        types.KeyboardButton('btcusdt'),
        types.KeyboardButton('ethusdt'),
        types.KeyboardButton('xrpusdt'),
        types.KeyboardButton('ltcusdt'),
    ]
    keyboard.add(*buttons)
    return keyboard

@bot.message_handler(commands=['start', 'help'])
def start(message):
    help_text = (
        'Привет! Я бот для отображения курса криптовалют.\n\n'
        'Используй команду /crypto или выбери криптовалюту кнопкой.\n\n'
        'Поддерживаемые символы криптовалют:\n'
        'btcusdt - Bitcoin в USDT\n'
        'ethusdt - Ethereum в USDT\n'
        'xrpusdt - Ripple в USDT\n'
        'ltcusdt - Litecoin в USDT\n'
        'Другие токены тоже поддерживаются\n'
        'Product by MinVin (Igor Minenkov)\n'
        'Работает на Libreto Cloud'
    )
    bot.reply_to(message, help_text, reply_markup=create_crypto_keyboard())

@bot.message_handler(commands=['crypto'])
def crypto(message):
    if len(message.text.split()) == 1:
        bot.reply_to(message, 'Пожалуйста, укажите символ криптовалюты. Например, /crypto btcusdt')
        return

    crypto_symbol = message.text.split()[1].upper()

    try:
        price = get_crypto_price(crypto_symbol)
        bot.reply_to(message, f'Текущий курс {crypto_symbol.upper()}: ${price}', reply_markup=create_crypto_keyboard())
    except Exception as e:
        bot.reply_to(message, f'Произошла ошибка: {e}', reply_markup=create_crypto_keyboard())

@bot.message_handler(func=lambda message: True)
def handle_buttons(message):
    if message.text in ['btcusdt', 'ethusdt', 'xrpusdt', 'ltcusdt']:
        crypto_symbol = message.text.upper()
        try:
            price = get_crypto_price(crypto_symbol)
            bot.reply_to(message, f'Текущий курс {crypto_symbol.upper()}: ${price}', reply_markup=create_crypto_keyboard())
        except Exception as e:
            bot.reply_to(message, f'Произошла ошибка: {e}', reply_markup=create_crypto_keyboard())

if __name__ == '__main__':
    bot.polling(none_stop=True)