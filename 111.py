from PyQt6.QtWidgets import QApplication, QWidget, QVBoxLayout, QLabel, QLineEdit, QPushButton, QHBoxLayout, QMessageBox
from PyQt6.QtCore import QTimer, QThread, pyqtSignal
import ccxt


class CryptoApp(QWidget):
    def __init__(self):
        super().__init__()

        self.symbol_input = QLineEdit(self)
        self.result_label = QLabel(self)
        self.result_label.setFixedHeight(44)

        layout = QVBoxLayout(self)
        layout.addWidget(self.symbol_input)

        layout.addWidget(QPushButton('Выполнить', clicked=self.on_button_click))
        
        help_button = QPushButton('Справка', clicked=self.display_help)
        layout.addWidget(help_button)

        layout.addWidget(self.result_label)

        crypto_buttons_layout = QHBoxLayout()
        cryptocurrencies = ['BTC', 'ETH', 'XRP', 'LTC']
        for crypto in cryptocurrencies:
            button = QPushButton(crypto, clicked=lambda checked, c=crypto: self.get_crypto_price_and_display(c))
            crypto_buttons_layout.addWidget(button)

        layout.addLayout(crypto_buttons_layout)

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_crypto_prices)
        self.timer.start(30000)

        self.threads = []

    def display_help(self):
        help_text = (
            'Привет! Я программа для отображения курса криптовалют.\n\n'
            'Используй команду /crypto "символ", чтобы получить курс.\n\n'
            'Поддерживаемые символы криптовалют:\n'
            'btcusdt - Bitcoin в USDT\n'
            'ethusdt - Ethereum в USDT\n'
            'xrpusdt - Ripple в USDT\n'
            'ltcusdt - Litecoin в USDT\n'
            'Другие токены тоже поддерживаются\n'
            'Product by MinVin (Igor Minenkov)'
        )
        self.show_popup("Справка", help_text)

    def get_crypto_price(self, symbol):
        exchange = ccxt.binance()
        ticker = exchange.fetch_ticker(symbol + 'USDT')
        return ticker['last']

    def get_crypto_price_and_display(self, symbol):
        crypto_thread = CryptoThread(self.fetch_and_display_price, symbol)
        crypto_thread.error.connect(self.show_error_popup)
        crypto_thread.finished.connect(crypto_thread.deleteLater)
        self.threads.append(crypto_thread)
        crypto_thread.start()

    def fetch_and_display_price(self, symbol):
        try:
            price = self.get_crypto_price(symbol)
            self.result_label.setText(f'Текущий курс {symbol.upper()}: ${price}')
        except Exception as e:
            self.show_error_popup(f'Произошла ошибка: {e}')

    def on_button_click(self):
        user_input = self.symbol_input.text().strip()

        if user_input.lower() in ('/start', '/help'):
            self.display_help()
        elif user_input.startswith('/crypto '):
            crypto_symbol = user_input.split()[1].upper()
            self.get_crypto_price_and_display(crypto_symbol)
        else:
            self.show_error_popup('Некорректная команда. Введите /help для отображения справки.')

    def show_popup(self, title, content):
        popup = QMessageBox(self)
        popup.setWindowTitle(title)
        popup.setText(content)
        popup.setFixedSize(400, 200)
        popup.show()

    def show_error_popup(self, message):
        self.show_popup("Ошибка", message)

    def update_crypto_prices(self):
        for crypto in ['BTC', 'ETH', 'XRP', 'LTC']:
            update_thread = CryptoThread(self.fetch_and_update_button, crypto)
            update_thread.error.connect(self.show_error_popup)
            update_thread.finished.connect(update_thread.deleteLater)
            self.threads.append(update_thread)
            update_thread.start()

    def fetch_and_update_button(self, crypto):
        try:
            price = self.get_crypto_price(crypto)
            button = self.findChild(QPushButton, crypto)
            if button:
                button.setText(f'{crypto} ${price:.2f}')
        except Exception as e:
            print(f'Error updating {crypto} price: {e}')


class CryptoThread(QThread):
    error = pyqtSignal(str)

    def __init__(self, target, *args):
        super().__init__()
        self.target = target
        self.args = args

    def run(self):
        try:
            self.target(*self.args)
        except Exception as e:
            self.error.emit(str(e))


if __name__ == '__main__':
    app = QApplication([])
    crypto_app = CryptoApp()
    crypto_app.show()
    app.exec()
    for thread in crypto_app.threads:
        thread.wait()
